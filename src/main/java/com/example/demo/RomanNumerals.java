package com.example.demo;


import java.util.Arrays;
import java.util.List;

public class RomanNumerals {
    private final List<String> roman = Arrays.asList("I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X");

    public String toRoman(int number) throws IndexOutOfBoundsException {
        if (number < 1 || number > 10) throw new IndexOutOfBoundsException("Number must be from 1 to 10");
        return roman.get(number - 1);

    }

}
