package com.example.demo;

public class MessageService {
    private INetwork network;

    public MessageService(INetwork network) {
        this.network = network;
    }

    public boolean sendMessage(String ip ,String message){
            boolean hasSent = network.sendMessage(ip, message);
            if (!hasSent){
                hasSent = network.sendMessage(ip,message);
            }
            return hasSent;
    }

}
