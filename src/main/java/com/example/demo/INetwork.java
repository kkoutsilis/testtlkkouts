package com.example.demo;

public interface INetwork {

    boolean sendMessage(String ip, String message);
}
