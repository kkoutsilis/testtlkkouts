package com.example.demo;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class RomanNumeralsTest {
    RomanNumerals romanNumerals;

    @Before
    public void setUp() throws Exception {
        romanNumerals = new RomanNumerals();
    }

    @After
    public void tearDown() throws Exception {
        romanNumerals = null;
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testToRomanWhenInputLessThanZero() {
        romanNumerals.toRoman(-1);
        fail("IndexOutOfBoundsException not thrown when input less than zero");
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testToRomanWhenInputEqualsZero() {
        romanNumerals.toRoman(0);
        fail("IndexOutOfBoundsException not thrown when input = 0 ");
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testToRomanWhenInputGreaterThanTen() {
        romanNumerals.toRoman(15);
        fail("IndexOutOfBoundsException not thrown when input greater than 10 ");
    }


    @Test
    public void testToRomanWhenInputEqualsOne() {
        assertEquals("I", romanNumerals.toRoman(1));
    }

    @Test
    public void testToRomanWhenInputEqualsTwo() {
        assertEquals("II", romanNumerals.toRoman(2));
    }

    @Test
    public void testToRomanWhenInputEqualsThree() {
        assertEquals("III", romanNumerals.toRoman(3));
    }

    @Test
    public void testToRomanWhenInputEqualsFour() {
        assertEquals("IV", romanNumerals.toRoman(4));
    }

    @Test
    public void testToRomanWhenInputEqualsFive() {
        assertEquals("V", romanNumerals.toRoman(5));
    }

    @Test
    public void testToRomanWhenInputEqualsSix() {
        assertEquals("VI", romanNumerals.toRoman(6));
    }

    @Test
    public void testToRomanWhenInputEqualsSeven() {
        assertEquals("VII", romanNumerals.toRoman(7));
    }

    @Test
    public void testToRomanWhenInputEqualsEight() {
        assertEquals("VIII", romanNumerals.toRoman(8));
    }

    @Test
    public void testToRomanWhenInputEqualsNine() {
        assertEquals("IX", romanNumerals.toRoman(9));
    }

    @Test
    public void testToRomanWhenInputEqualsTen() {
        assertEquals("X", romanNumerals.toRoman(10));
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testToRomanWhenInputEqualsEleven() {
        romanNumerals.toRoman(11);
        fail("IndexOutOfBoundsException not thrown when input = 11 ");
    }

}