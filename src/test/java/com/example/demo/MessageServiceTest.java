package com.example.demo;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.junit.Assert.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class MessageServiceTest {

    @InjectMocks
    private MessageService messageService;

    @Mock
    private INetwork network;

    @Before
    public void setUp() throws Exception {
        messageService = new MessageService(network);
        network = new FakeNetwork();
    }

    @After
    public void tearDown() throws Exception {
        messageService = null;
        network = null;
    }

    @Test
    public void testSendMessageWhenEverythingIsOk() {
        when(network.sendMessage(anyString(),anyString())).thenReturn(true);
        assertTrue(messageService.sendMessage("192.168.1.1","Hello"));
    }

    @Test
    public void testSendMessageWhenNetworkDown() {
        when(network.sendMessage(anyString(),anyString())).thenReturn(false);
        assertFalse(messageService.sendMessage("192.168.1.1","Hello"));
    }
    @Test
    public void testSendMessageCallsMethod2Times() {
        when(network.sendMessage(anyString(),anyString())).thenReturn(false);
        messageService.sendMessage("192.168.1.1","Hello");
        verify(network, times(2)).sendMessage(anyString(),anyString());

    }
}